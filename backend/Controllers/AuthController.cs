using backend.Data.Interface;
using backend.Models;
using backend.Service;
using BC = BCrypt.Net.BCrypt;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace backend.Controllers
{
    [ApiController]
    [Route("api")]
    public class AuthController : ControllerBase
    {
        private readonly IUser _repository;
        private readonly JwtService _jwtService;

        public AuthController(IUser context, JwtService service) 
        {
            _repository = context;
            _jwtService = service;
        } 
        [HttpPost("signUp")]
        public async Task<ActionResult> SignUp([FromBody] Users user) 
        {
            Users account = await _repository.FindEmail(user.Email);
            if (account != null)
            {
                return BadRequest(new { error = "Данный email уже существует!" });
            }
            // Замена пароля 
            user.Password = BC.HashPassword(user.Password);     
            return Ok( await _repository.CreateUser(user));
        }

        [HttpPost("signIn")]
        public async Task<ActionResult> SignIn([FromBody] Users user) 
        {
            Users account = await _repository.FindEmail(user.Email);
            if (account == null || !BC.Verify(user.Password, account.Password)) 
            {
                return BadRequest(new { error = "Не верный логин или пароль" });
            }
            // Генерация JWT токена на выходе 
            string tokenString = _jwtService.GenerateJSONWebToken(user);    
            return Ok(new { token = tokenString });
        }

        [Authorize] // добавляем Middleware на JWT Bears (в случае отсутствия токена - ошибка 401 )
        [HttpGet("users")]
        public async Task<ActionResult> GetUsers()
        {
            List<Users> users = await _repository.GetAllUsers();
            return Ok(users);
        }
    }
}
