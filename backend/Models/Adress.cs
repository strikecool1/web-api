using System;
using System.ComponentModel.DataAnnotations;

namespace backend.Models
{
    public class Adress
    {
        [Key]
        public long Id { get; set; }
        [Required]
        [MaxLength(500)]
        public string Street { get; set; }
        [Required]
        [MaxLength(200)]
        public string City { get; set; }
    }
}