using System.ComponentModel.DataAnnotations;

namespace backend.Models
{
    public class Users
    {
        [Key]
        public long Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        [MaxLength(250)]
        public string Email { get; set; }
        [Required]
        [MaxLength(250),MinLength(5)]
        public string Password { get; set; }
        [Required]
        public bool IsConfirm { get; set; }

    }

}