using backend.Models;
using Microsoft.EntityFrameworkCore;

namespace backend.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options): base(options){ }
        public DbSet<Users> Users { get; set; }
        public DbSet<Persons> People { get; set; }
        public DbSet<Adress> Adresses { get; set; }

    }

}