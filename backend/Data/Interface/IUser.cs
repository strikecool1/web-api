using System.Collections.Generic;
using System.Threading.Tasks;
using backend.Models;

namespace backend.Data.Interface
{
    public interface IUser
    {
        Task<List<Users>> GetAllUsers();
        Task<Users> GetUser(int id);
        Task<Users> FindEmail(string email);
        Task<Users> CreateUser(Users user);
        Task<Users> DeleteUser(Users user);
        Task<Users> UpdateUser(Users user);
    }
}