using backend.Data.Interface;
using backend.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace backend.Data.Repository 
{
    public class UserRepository : IUser 
    {
        private readonly ApplicationContext _Context;
        public UserRepository(ApplicationContext db) => _Context = db;
        public async Task<List<Users>> GetAllUsers() => await _Context.Users.ToListAsync();
        public async Task<Users> GetUser(int id) => await _Context.Users.FirstOrDefaultAsync(x => x.Id == id);
        public async Task<Users> FindEmail(string email) => await _Context.Users.FirstOrDefaultAsync(x => x.Email == email);
        public async Task<Users> CreateUser(Users user)
        {
            _Context.Users.Add(user);
            await _Context.SaveChangesAsync();
            return user;
        }
        public async Task<Users> DeleteUser(Users user)
        {
            _Context.Users.Remove(user);
            await _Context.SaveChangesAsync();
            return user;
        }
        public async Task<Users> UpdateUser(Users user) 
        {
            _Context.Users.Update(user);
            await _Context.SaveChangesAsync();
            return user;
        }
    }
}