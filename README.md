# web-api
## Основные используемые технологии и их версии ##
### Backend (WebApi) ###
* aspnet:5.0-alpine
* .net:5.0-alpine
### DataBase ###
* Postgres 13-alpine:3.14
* PgAdmin dpage/pgadmin4
***
## Предварительная настройка ##
Для компиляции проекта необходимо ввести следующую команду:

    docker-compose up -d --build

После чего можно удалить все остальные промежуточные контейнеры через команду:

    docker system prune -a

По итогу останиться только 3 контейнера "api-container","db-container" и pgadmin.  
## Миграции ##
Для запуска миграций необходимо установить пакет "ef" для этого необходимо ввести следующую команду:

    dotnet tool install --global dotnet-ef

После чего применить сами миграции (директория "/Migrations") следующей командой:

    dotnet ef database update

Если необходимо добавить новую миграции:

    dotnet ef migrations add InitialCreate
